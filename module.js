import { Table } from "./index.js";

const dataTable = new Table({
    header: ["Name", "Email"],
    body: [
            ["Dian Aditya", "dian.aditya53@gmail.com"], 
            ["Joko Winarno", "joko25@gmail.com"], 
            ["Bambang Pamungkas", "bp1993@gmail.com"],
            ["Jane Doe", "janedoegaming@yahoo.com"],
            ["John Doe", "johndoez@outlook.com"],
            ["Burhan Tu", "superowl@yahoo.com"]
        ]
});

const app = document.getElementById("app");

dataTable.render(app);

